# Something in Common

Desc: `We've managed to gather the details in the given file. We need a professional cryptographer to decipher the message. Are you up to the task?`

Given files:

* `rsa_details.txt`

Flag: `TUCTF{Y0U_SH0ULDNT_R3US3_TH3_M0DULUS}`
