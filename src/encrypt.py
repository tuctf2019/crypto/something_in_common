#!/usr/bin/env python

from Crypto.Util import number
import binascii

def egcd(a, b):
    if a == 0:
        return (b, 0, 1)
    else:
        g, y, x = egcd(b % a, a)
        return (g, x - (b // a) * y, y)

def modinv(a, m):
    g, x, y = egcd(a, m)
    if g != 1:
        raise Exception('modular inverse does not exist')
    else:
        return x % m


# e1 = 65537
# e2 = 68281
e1 = 15
e2 = 13

NUM_BITS = 256

p = number.getPrime(NUM_BITS)
q = number.getPrime(NUM_BITS)
n = p * q

print(f'n = {n}\n\n')

tot = (p - 1) * (q - 1)

d1 = modinv(e1, tot)
d2 = modinv(e2, tot)

FLAG = 'TUCTF{Y0U_SH0ULDNT_R3US3_TH3_M0DULUS}'
print(binascii.hexlify(FLAG.encode('utf-8')))

m = int(binascii.hexlify(FLAG.encode('utf-8')), 16)
print(f'm = {m}\n\n')

c1 = pow(m, e1, n)
c2 = pow(m, e2, n)

print(f'c1 = {c1}\n\nc2 = {c2}\n\n')
