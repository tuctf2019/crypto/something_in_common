# Something in Common -- TUCTF 2019

The idea is to recognize that the only RSA value that does not have two is the modulus. This implies that the modulus must be shared between the two ciphertexts. This allows for a common modulus attack. A script for this can easily be found online.

Solve Script: `attack.py`

Flag: `TUCTF{Y0U_SH0ULDNT_R3US3_TH3_M0DULUS}`
